import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    store.dispatch("setUsername", "");
    this.$router.push({ path: "/" });
  }
}).$mount("#app");
